import __main__
from globales import *

# Variable global que va contando la cantidad de espacios que tiene que tener el debugger
cantidad_de_espacios = 0
mostrar_prints = False

def mostrar_ocultar_prints(estado=None):
	global mostrar_prints
	# Si no pasan parámetros, cambio el estado
	if estado is None:
		mostrar_prints = not mostrar_prints
	# Si pasan lo seteo
	elif estado is True or estado is False:
		mostrar_prints = estado
	else:
		raise Exception

# Función para registrar / imprimir en pantalla cuando otra función F empieza y termina.
# Para eso arrobo @degub una línea antes de definir F (asigno a debug como decorator de F). 
def debug(funcion):

	def registro_funcion(*args, **kwargs):

		global cantidad_de_espacios
		global mostrar_prints

		# Tomo elementos que voy a registrar: nombre de la función y espacios para legibilidad del debug
		espacios = "".join([" " for espacio in range(cantidad_de_espacios + 1)])
		cantidad = str(cantidad_de_espacios)
		nombre = funcion.__name__

		# Obtengo el archivo donde está la función, en general __module__ debería bastar, pero si la función justo está
		# definida en el archivo que uso de cabecera al llamar por consola a python, me va a devolver __main__
		modulo = funcion.__module__
		# Si justo el módulo es main, pido su archivo, si no el módulo va a coincidir con el nombre del archivo
		archivo = __main__.__file__ if modulo == "__main__" else modulo + ".py"

		# Obtengo las líneas para registrar inicio y fin de una función
		inicio_funcion = espacios + cantidad + " Inicio de '" + nombre + "' en '" + archivo + "'."
		fin_funcion = espacios + cantidad + " Fin de '" + nombre + "' en '" + archivo + "'."

		# Aumento en 1 la cantidad de espacios para llamadas a funcion dentro de la F que estoy registrando
		cantidad_de_espacios += 1

		# Imprimo/loggeo el inicio de la función
		if mostrar_prints:
			print(inicio_funcion)

		# Ejecuto la función
		resultado = funcion(*args, **kwargs)

		# Imprimo/loggeo el fin de la función
		if mostrar_prints:
			print(fin_funcion)

		# Decremento en 1 la cantidad de espacios
		cantidad_de_espacios -= 1

		# Finalmente devuelvo el resultado de la F
		return resultado

	# Hago que se ejecute registro_funcion con los argumentos de la F original
	return registro_funcion


# Una petición es una función que inicia en el bot porque un usuario realizó algo, ya sea mandar un comando, 
# un mensaje común, un cambio en algún grupo (cambiar foto, nombre, eliminar un usuario, etc), aprear un botón, etc
def chequeos_peticion(funcion):

	# Ejecuto todo lo necesario al llamar a un comando
	def inicios_de_comando(*args, **kwargs):

		# Obtengo los argumentos
		bot = args[0]
		update = args[1]
		chat = update.message.chat

		# Obtengo parámetros que voy a usar
		mensaje_id = update.message.message_id
		chat_id = update.message.chat_id
		user_id = update.message.from_user.id
		nombre_func = funcion.__name__
		
		nombre_chat = chat.title if chat.type != "private" else chat.first_name + (" " + chat.last_name if chat.last_name else "")

		# Obtengo el archivo
		modulo = funcion.__module__
		archivo = __main__.__file__ if modulo == "__main__" else modulo + ".py"

		# Armo mensajes de inicio y fin
		inicio = "\n----INICIO COMANDO QUE EJECUTA FUNCIÓN: '" + nombre_func + "' en archivo: '" + archivo + "' por mensaje: '" + str(mensaje_id) + "' en chat: '" + nombre_chat + "' (" + str(chat_id) + ")----\n"
		fin = "\n----FIN COMANDO QUE EJECUTÓ FUNCIÓN: '" + nombre_func + "' en archivo: '" + archivo + "' por mensaje: '" + str(mensaje_id) + "' en chat: '" + nombre_chat + "' (" + str(chat_id) + ")----\n"

		# Handleo excepciones para registrar algún error ocurrido
		try:
			# Imprimo inicio de comando
			if mostrar_prints:
				print(inicio)	
 
			# Si es un usuario prohibido, no puedo dejar que se ejecute la función
			if update.message.chat_id == mi_chat_id:
				funcion(*args, **kwargs)

			# Imprimo fin comando
			if mostrar_prints:
				print(fin)

		except Exception as e:
			# Actualizo cantidad_de_espacios
			cantidad_de_espacios = 0
			# Imprimo y mando línea de debuggeo
			excepcion = "\n\n----->>>>> Excepción ocurrida mientras se ejecutaba '" + nombre_func + "' en archivo '" + archivo + "' <<<<<-----\n\n" + str(e) + "\n\n"
			print(excepcion)
			try:
				bot.send_message(chat_id=chat_id, texto="Zarpado error mostro, ojalá haya sido de conexión nomás\n" + mi_username + "arreglá esto salame")
			except Exception as e2:
				print("Mientras trataba de mandar un mensaje para avisar del error surgió otra excepción:\n" + str(e2))
	# Lo primero que hago es llamar a inicios comando para que tome los parámetros originales
	return inicios_de_comando