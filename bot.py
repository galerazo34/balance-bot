# Importamos las bibliotecas necesarias
from telegram.ext import Updater, MessageHandler, Filters, CommandHandler, CallbackQueryHandler

# Inicialización
import database
from globales import *
from debug import *


def main():

	try:

		print("\n----INICIALIZAR TODO----")
	
		print("\n>> Inicio de 'main' en 'bot.py' <<\n")

		# Seteo de estructuras iniciales
		mostrar_ocultar_prints()
		database.conectar_db()
	
		# Creamos el Updater, objeto que se encargará de mandarnos las peticiones del bot
		# Por supuesto no hay que olvidarse de cambiar donde pone "TOKEN" por el token que da BotFather
		updater = Updater(token)
	
		# Seteamos el Dispatcher, en el cual registraremos los comandos del bot y su funcionalidad
		dispatcher = updater.dispatcher
		agregar_comandos_y_filtros(dispatcher)
		
	
		print("\n>> Fin de 'main' en 'bot.py' <<\n")
	
		print("\n----BOT INICIALIZADO----\n")
	
		# Y comenzamos la ejecución del bot a las peticiones
		updater.start_polling()
		updater.idle()

	except Exception as e:
		print(">>>>>>>>>> Excepción FATAL, no se pudo iniciar el bot <<<<<<<<<<\n\n" + str(e))


@debug
def agregar_comandos_y_filtros(dispatcher):
	# Agrego comandos
	dispatcher.add_handler(CommandHandler("start", start), group=0)
	dispatcher.add_handler(CommandHandler("info", info), group=0)

	dispatcher.add_handler(CommandHandler("saldo", saldo), group=0)
	dispatcher.add_handler(CommandHandler("deudas", deudas), group=0)

def start(bot, update):
	bot.send_message(chat_id=update.message.chat_id, text="¡Hola! El bot está andando.")

@chequeos_peticion
def info(bot, update):
	bot.send_message(chat_id=update.message.chat_id, text="Completar")

@chequeos_peticion
def saldo(bot, update):
	bot.send_message(chat_id=update.message.chat_id, text="Completar")

@chequeos_peticion
def deudas(bot, update):
	bot.send_message(chat_id=update.message.chat_id, text="Completar")






















































# Llamamos al método main para ejecutar lo anterior
if __name__ == '__main__':
	main()